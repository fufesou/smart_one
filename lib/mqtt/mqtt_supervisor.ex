defmodule SmartOne.MqttSupervisor do
  use Supervisor

  def start_link() do
    Supervisor.start_link(__MODULE__, [])
  end

  def init([]) do
    children = [
      worker(SmartOne.MqttClient, [self()])
    ]
    supervise(children, strategy: :one_for_one)
  end

  # def start_link() do
  #   result = {:ok, pid} = Supervisor.start_link(__MODULE__, [])
  #   start_workers(pid)
  #   result
  # end

  # def start_workers(sup) do
  #   Supervisor.start_child(sup, worker(SmartOne.MqttClient, []))
  # end

  # def init(_) do
  #   supervise [], strategy: :one_for_one
  # end

end

