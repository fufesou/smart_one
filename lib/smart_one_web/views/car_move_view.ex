defmodule SmartOneWeb.CarMoveView do
  use SmartOneWeb, :view

  def render("move_to.json", %{pages: pages}) do
    %{data: %{direction: pages.direction, direction_code: pages.direction_code}}
  end

end
