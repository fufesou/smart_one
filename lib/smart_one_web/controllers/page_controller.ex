defmodule SmartOneWeb.PageController do
  use SmartOneWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
