# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :smart_one,
  ecto_repos: [SmartOne.Repo]

# Configures the endpoint
config :smart_one, SmartOneWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "kzs2FhZgx0u30EtVCYAve5vAhaM0uwowvZF736O5bpt+g7heAg6Glu/XvZSugl/Y",
  render_errors: [view: SmartOneWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: SmartOne.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
